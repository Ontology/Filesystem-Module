﻿Imports System.Reflection
Imports Ontology_Module
Imports System.Security
Imports OntologyClasses
Imports OntologyClasses.BaseClasses
Imports OntologyAppDBConnector
Imports OntoMsg_Module
Imports System.Runtime.InteropServices
Imports MediaStore_Module

Public Class clsBlobConnection
    Private objLocalConfig As clsLocalConfig

    Private objFileWorkManager As FileWorkManager
    Private objMediaStoreConnector As MediaStoreConnector
    'Private objDBLevel_Blobs As OntologyModDBConnector
    'Private objDataWork As clsDataWork

    'Private objTransaction_Files As clsTransaction_Files
    'Private objTransaction As clsTransaction
    'Private objRelationConfig As clsRelationConfig

    Private objFrmBlobWatcher As frmBlobWatcher

    'Private objStream_Read As IO.Stream
    'Private byteFile() As Byte
    'Private strHash_File As String
    'Private strPath_Blob As String
    'Private strPathBlobWatch As String
    'Private boolBlobActive As Boolean
    'Private boolBlobWatcherConfigured As Boolean

    'Private objFileInfo As IO.FileInfo

    Public ReadOnly Property LocalConfig As clsLocalConfig
        Get
            Return objLocalConfig
        End Get
    End Property

    Public ReadOnly Property OItem_Class_File As clsOntologyItem
        Get
            Return objLocalConfig.OItem_class_filesync
        End Get
    End Property

    Public ReadOnly Property FileInfoBlob As IO.FileInfo
        Get
            Return objMediaStoreConnector.FileInfo
        End Get
    End Property


    Public ReadOnly Property Path_Blob As String
        Get
            Return objMediaStoreConnector.MediaPath
        End Get
    End Property
    Public ReadOnly Property Path_BlobWatcher As String
        Get
            Return objMediaStoreConnector.MediaWatcherPath
        End Get
    End Property
    Public ReadOnly Property BlobActive As Boolean
        Get
            Return objMediaStoreConnector.IsMediaPathSet
        End Get
    End Property

    Public ReadOnly Property BlobWatchConfigured As Boolean
        Get
            Return objMediaStoreConnector.IsMediaWatcherPathSet
        End Get
    End Property

    Public Sub start_BlobDirWatcher()
        Dim objOItem_Result As clsOntologyItem

        If objFrmBlobWatcher Is Nothing Then
            objFrmBlobWatcher = New frmBlobWatcher(objLocalConfig.Globals)
        End If

        If Not objFrmBlobWatcher.IsHandleCreated Then
            objOItem_Result = objFrmBlobWatcher.Initialize_BlobDirWatcher()
            If Not objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
                objFrmBlobWatcher.Hide()
            End If

        End If
    End Sub

    Public Function del_Blob(OItem_File As clsOntologyItem) As clsOntologyItem
        Return objMediaStoreConnector.DelMedia(OItem_File)
    End Function


    Public Function compute_Hashes() As clsOntologyItem
        Return objMediaStoreConnector.ComputerHashes()
    End Function

    Public Function isFilePresent(strPath_File As String) As clsOntologyItem
        Return objMediaStoreConnector.IsFilePresent(strPath_File)
    End Function

    Public Function save_File_To_Blob(ByVal objOItem_File As clsOntologyItem, ByVal strPath_File As String) As clsOntologyItem
        Return objMediaStoreConnector.SaveFileToManagedMedia(objOItem_File, strPath_File)
    End Function
    Public Function get_BlobSTream(ByVal objOItem_File As clsOntologyItem) As IO.Stream
        Return objMediaStoreConnector.GetManagedMediaStream(objOItem_File)
    End Function

    Public Function GetBlobPath(objOItem_File As clsOntologyItem) As String
        Return objMediaStoreConnector.GetMediaBlobPath(objOItem_File)
    End Function

    Private Function GetSubFolderPath(strGUID_File As String) As String
        Return objMediaStoreConnector.GetSubFolderPath(strGUID_File)
    End Function

    Public Function GetBlobPath(strGUID_File As String) As String
        Return objMediaStoreConnector.GetMediaBlobPath(strGUID_File)
    End Function

    Public Function save_Blob_To_File(ByVal objOItem_File As clsOntologyItem, ByVal strPath_File As String, Optional boolOverwrite As Boolean = False) As clsOntologyItem
        Return objMediaStoreConnector.SaveManagedMediaToFile(objOItem_File, strPath_File, boolOverwrite)
    End Function

    Private Function get_File_Of_Hash(ByVal strHash As String) As clsOntologyItem
        Return objMediaStoreConnector.GetFileOfHash(strHash)
    End Function

    Public Function get_Hash_Of_File(ByVal strPath As String) As String
        Return objMediaStoreConnector.GetHashOfFile(strPath)
    End Function

    Public Function get_Hash_Of_DBFile(ByVal objOItem_File As clsOntologyItem) As clsOntologyItem
        Return objMediaStoreConnector.GetHashOfManagedMedia(objOItem_File)
    End Function

    Public Function save_Hash_Of_DBFile(objOItem_File As clsOntologyItem) As clsOntologyItem
        Return objMediaStoreConnector.SaveHashOfManagedMedia(objOItem_File)
    End Function

    Public Function compare_Files(ByVal strFilePath_SRC As String, ByVal strFilePath_DST As String) As clsOntologyItem
        Return objMediaStoreConnector.CompareFiles(strFilePath_SRC, strFilePath_DST)
    End Function

    Public Function SearchFileByIdentity(strPath As String) As clsOntologyItem

        Dim objResult = objMediaStoreConnector.SearchFileByIdentity(strPath)

        If (objResult.GUID = objLocalConfig.Globals.LState_Success.GUID) Then
            Return objResult.OList_Rel.FirstOrDefault()
        Else
            Return objResult
        End If
    End Function

    Private Function GetGuidOfFSFile(strPath As String) As clsOntologyItem
        Return objMediaStoreConnector.GetGuidOfFSFile(strPath)
    End Function

    Public Function WriteIdentityToFile(OItem_File As clsOntologyItem, strPath As String) As clsOntologyItem
        Return objMediaStoreConnector.WriteIdentityToFile(OItem_File, strPath)
    End Function


    Public Sub New()
        objLocalConfig = LocalConfigManager.GetLocalConfig(DirectCast(Assembly.GetExecutingAssembly().GetCustomAttributes(True).FirstOrDefault(Function(objAttribute) TypeOf (objAttribute) Is GuidAttribute), GuidAttribute).Value)
        If objLocalConfig Is Nothing Then
            objLocalConfig = New clsLocalConfig(New Globals)
            LocalConfigManager.AddLocalConfig(objLocalConfig)
        End If

        set_DBConnection()
        initialize()
    End Sub

    Public Sub New(ByVal Globals As Globals)
        objLocalConfig = LocalConfigManager.GetLocalConfig(DirectCast(Assembly.GetExecutingAssembly().GetCustomAttributes(True).FirstOrDefault(Function(objAttribute) TypeOf (objAttribute) Is GuidAttribute), GuidAttribute).Value)
        If objLocalConfig Is Nothing Then
            objLocalConfig = New clsLocalConfig(Globals)
            LocalConfigManager.AddLocalConfig(objLocalConfig)
        End If

        set_DBConnection()
        initialize()
    End Sub

    Private Sub initialize()
        objFileWorkManager = New FileWorkManager(objLocalConfig.Globals)
        objMediaStoreConnector = New MediaStoreConnector(objLocalConfig.Globals)
    End Sub

    Public Sub New(ByVal LocalConfig As clsLocalConfig)
        objLocalConfig = LocalConfig

        set_DBConnection()
        initialize()
    End Sub

    Private Sub set_DBConnection()

    End Sub
End Class
