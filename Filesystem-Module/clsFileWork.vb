﻿Imports System.Reflection
Imports Ontology_Module
Imports ClassLibrary_ShellWork
Imports OntologyClasses.BaseClasses
Imports Ionic
Imports OntologyAppDBConnector
Imports OntoMsg_Module
Imports System.Runtime.InteropServices
Imports MediaStore_Module

Public Class clsFileWork


    Public Property objLocalConfig As clsLocalConfig
    Private objTransaction As clsTransaction
    Private objRelationConfig As clsRelationConfig


    Private objMediaStoreConnector As MediaStoreConnector
    Private objFileWorkManager As FileWorkManager

    Private objDBLevel_Blob As OntologyModDBConnector
    Private objDBLevel_FSO As OntologyModDBConnector
    Private objDBLevel_Folder As OntologyModDBConnector
    Private objDBLevel_Server As OntologyModDBConnector
    Private objDBLevel_Share As OntologyModDBConnector
    Private objDBLevel_Drive As OntologyModDBConnector
    Private objDBLevel_CreationDate As OntologyModDBConnector

    Private objOItem_Drive As clsOntologyItem
    Private objOItem_Server As clsOntologyItem
    Private objOItem_Share As clsOntologyItem

    Private objShellWork As clsShellWork
    Private objFrmBlobWatcher As frmBlobWatcher
    Private objDataWork As clsDataWork

    Private boolBlob As Boolean
    Private strSeperator As String

    Private objOList_FolderHierarchy As List(Of clsObjectTree)
    Private objOList_Folders As List(Of clsOntologyItem)

    Private objZipFile As Zip.ZipFile

    Public ReadOnly Property OItem_Class_File As clsOntologyItem
        Get
            Return objLocalConfig.OItem_Type_File
        End Get
    End Property

    Public ReadOnly Property OItem_Class_Folder As clsOntologyItem
        Get
            Return objLocalConfig.OItem_type_Folder
        End Get
    End Property

    Public ReadOnly Property OItem_Class_Drive As clsOntologyItem
        Get
            Return objLocalConfig.OItem_Type_Drive
        End Get
    End Property

    Public ReadOnly Property OItem_Class_Server As clsOntologyItem
        Get
            Return objLocalConfig.OItem_Type_Server
        End Get
    End Property

    Public ReadOnly Property LocalConfig As clsLocalConfig
        Get
            Return objLocalConfig
        End Get
    End Property

    Public Function GetCreationDatesOfFiles(OList_Files As List(Of clsOntologyItem)) As List(Of clsObjectAtt)
        Dim OList_CreateDates As List(Of clsObjectAtt)

        Dim searchCreationDates = OList_Files.Select(Function(fileItem) New clsObjectAtt With {.ID_Object = fileItem.GUID,
                                                                                               .ID_AttributeType = objLocalConfig.OItem_Attribute_Datetimestamp__Create_.GUID}).ToList()

        Dim result = objDBLevel_CreationDate.GetDataObjectAtt(searchCreationDates, doIds:=False)

        If result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
            OList_CreateDates = objDBLevel_CreationDate.ObjAtts
        Else
            OList_CreateDates = Nothing
        End If

        Return OList_CreateDates
    End Function

    Public Function copy_File(OItem_File As clsOntologyItem, strPathDst As String) As clsOntologyItem
        Dim objOItem_Result As clsOntologyItem
        Dim strPathSrc As String
        If is_File_Blob(OItem_File) Then
            objOItem_Result = objMediaStoreConnector.SaveManagedMediaToFile(OItem_File, strPathDst, False)
        Else
            strPathSrc = get_Path_FileSystemObject(OItem_File)
            If IO.File.Exists(strPathSrc) Then
                Try
                    IO.File.Copy(strPathSrc, strPathDst)
                    objOItem_Result = objLocalConfig.Globals.LState_Success
                Catch ex As Exception
                    objOItem_Result = objLocalConfig.Globals.LState_Error
                End Try
            Else
                objOItem_Result = objLocalConfig.Globals.LState_Nothing
            End If
        End If

        Return objOItem_Result
    End Function

    Public Function save_File(OItem_File As clsOntologyItem) As clsOntologyItem
        Dim objOitem_Result As clsOntologyItem


        If Not OItem_File.GUID_Parent = objLocalConfig.OItem_Type_File.GUID Then
            objOitem_Result = objLocalConfig.Globals.LState_Error
        Else
            objOitem_Result = objLocalConfig.Globals.LState_Success
        End If

        If objOitem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
            objTransaction.ClearItems()
            objOitem_Result = objTransaction.do_Transaction(OItem_File)

            If objOitem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
                objTransaction.rollback()
            End If
        End If

        Return objOitem_Result
    End Function

    Public Function del_File(OItem_File As clsOntologyItem) As clsOntologyItem
        Dim objOItem_Result As clsOntologyItem

        If Not OItem_File.GUID_Parent = objLocalConfig.OItem_Type_File.GUID Then
            objOItem_Result = objLocalConfig.Globals.LState_Error
        Else
            objOItem_Result = objLocalConfig.Globals.LState_Success
        End If

        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
            objTransaction.ClearItems()
            objOItem_Result = objTransaction.do_Transaction(OItem_File, False, True)

            If objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
                objTransaction.rollback()
            End If
        End If

        Return objOItem_Result
    End Function

    Public Function is_File_Blob(ByVal objOItem_File As clsOntologyItem) As Boolean
        Return objFileWorkManager.IsBlobFile(objOItem_File)
    End Function

    Public Function is_File_Blob(strGUID_File As String) As Boolean
        Return objFileWorkManager.IsBlobFile(strGUID_File)
    End Function

    Public Function get_FileSystemObject_By_Path(strPath As String, doCreate As Boolean) As clsOntologyItem
        Return objFileWorkManager.GetFileSystemObjectByPath(strPath, doCreate)
    End Function

    Public Function get_FileInfo(objOItem_File As clsOntologyItem) As System.IO.FileInfo
        Return objFileWorkManager.GetFileInfo(objOItem_File)
    End Function

    Private Function RelateFileSystemObjectToLastFolder(OItem_Folder As clsOntologyItem, OItem_FSO As clsOntologyItem, doCreate As Boolean) As clsOntologyItem
        Dim objSearch_FSO_To_Folder = New List(Of clsObjectRel) From {New clsObjectRel With {.ID_Other = OItem_Folder.GUID,
                                                                                                                             .Name_Object = OItem_FSO.Name,
                                                                                                                             .ID_RelationType = objLocalConfig.OItem_RelationType_isSubordinated.GUID}}
        Dim objOItem_Result = objDBLevel_FSO.GetDataObjectRel(objSearch_FSO_To_Folder, doIds:=False)
        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
            Dim objFSOs = objDBLevel_FSO.ObjectRels.Where(Function(fso) fso.Name_Object.ToLower() = OItem_FSO.Name.ToLower()).ToList()
            If objFSOs.Any() Then
                OItem_FSO.GUID = objFSOs.First().ID_Object
                objOItem_Result = OItem_FSO
            Else
                If doCreate Then
                    objTransaction.ClearItems()
                    OItem_FSO.GUID = objLocalConfig.Globals.NewGUID
                    objOItem_Result = objTransaction.do_Transaction(OItem_FSO)
                    If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
                        Dim objORel_FSO_To_Folder = objRelationConfig.Rel_ObjectRelation(OItem_FSO, OItem_Folder, objLocalConfig.OItem_RelationType_isSubordinated)
                        objOItem_Result = objTransaction.do_Transaction(objORel_FSO_To_Folder)
                        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
                            objOItem_Result = OItem_FSO
                        End If
                    End If
                Else
                    objOItem_Result = objLocalConfig.Globals.LState_Nothing.Clone()
                End If
            End If
        End If

        Return objOItem_Result
    End Function

    Private Function GetFolderHierarchy(strFolders As List(Of String), OItem_ParentFolder As clsOntologyItem, doCreate As Boolean) As clsOntologyItem
        Dim objOItem_Result = objLocalConfig.Globals.LState_Error.Clone()
        Dim strFolder = strFolders(0)
        Dim boolCreate As Boolean = False

        objOItem_Result = objDBLevel_Folder.GetDataObjectsTree(objLocalConfig.OItem_type_Folder, objLocalConfig.OItem_type_Folder, objLocalConfig.OItem_RelationType_isSubordinated)
        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
            objOList_FolderHierarchy = objDBLevel_Folder.ObjectTree
            objOList_Folders = objDBLevel_Folder.Objects1
            objOList_Folders.Concat(objDBLevel_Folder.Objects1)

            Dim folderList = objOList_Folders.Where(Function(f) f.GUID = OItem_ParentFolder.GUID).ToList()

            If folderList.Any() Then
                Dim objOItem_Folder = folderList.First().Clone()

                For i As Integer = 1 To strFolders.Count - 1
                    strFolder = strFolders(i)

                    Dim folderRelList = objOList_FolderHierarchy.Where(Function(f) f.ID_Object = objOItem_Folder.GUID And f.Name_Object_Parent.ToLower() = strFolder.ToLower()).ToList()
                    If folderRelList.Any() Then
                        objOItem_Folder = folderRelList.Select(Function(f) New clsOntologyItem With {.GUID = f.ID_Object_Parent,
                                                                                                     .Name = f.Name_Object_Parent,
                                                                                                     .GUID_Parent = objLocalConfig.OItem_type_Folder.GUID,
                                                                                                     .Type = objLocalConfig.Globals.Type_Object}).First()
                    Else

                        If doCreate Then
                            Dim objOItem_SubFolder = New clsOntologyItem With {.GUID = objLocalConfig.Globals.NewGUID,
                                                                       .Name = strFolder,
                                                                       .GUID_Parent = objLocalConfig.OItem_type_Folder.GUID,
                                                                       .Type = objLocalConfig.Globals.Type_Object}
                            objTransaction.ClearItems()

                            objOItem_Result = objTransaction.do_Transaction(objOItem_SubFolder)
                            If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
                                Dim objORel_SubFolder_To_Folder = objRelationConfig.Rel_ObjectRelation(objOItem_SubFolder, objOItem_Folder, objLocalConfig.OItem_RelationType_isSubordinated)
                                objOItem_Result = objTransaction.do_Transaction(objORel_SubFolder_To_Folder)
                                If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
                                    objOItem_Folder = objOItem_SubFolder
                                Else

                                    objTransaction.rollback()
                                    Exit For
                                End If
                            Else
                                Exit For
                            End If
                        Else
                            objOItem_Result = objLocalConfig.Globals.LState_Nothing.Clone
                            Exit For
                        End If

                    End If


                Next
                If Not objOItem_Folder Is Nothing Then
                    objOItem_Result = objOItem_Folder
                End If
            End If
        End If




        Return objOItem_Result
    End Function

    Public Function get_Path_FileSystemObject(ByVal objOItem_FileSystemObject As clsOntologyItem, Optional ByVal boolBlobPath As Boolean = True) As String
        Return objFileWorkManager.GetPathFileSystemObject(objOItem_FileSystemObject, boolBlob)
    End Function

    Private Function get_PathPart(ByVal strID_Folder As String) As clsOntologyItem
        Dim objRel = From obj In objDBLevel_Folder.ObjectTree
                     Where (obj.ID_Object_Parent = strID_Folder)

        Dim objOItem_PathPart As New clsOntologyItem

        If objRel.Count > 0 Then
            objOItem_PathPart.Name = objRel(0).Name_Object
            objOItem_PathPart.GUID = objRel(0).ID_Object
        End If

        Return objOItem_PathPart
    End Function

    Public Function merge_paths(ByVal strPath1 As String, ByVal strPath2 As String) As String
        Dim strPath As String


        If strPath1.EndsWith(IO.Path.DirectorySeparatorChar) Then
            If strPath2.StartsWith(IO.Path.DirectorySeparatorChar) Then
                strPath = strPath1 & strPath2.Substring(1)
            Else
                strPath = strPath1 & strPath2
            End If
        Else
            If strPath2.StartsWith(IO.Path.DirectorySeparatorChar) Then
                strPath = strPath1 & strPath2
            Else
                strPath = strPath1 & IO.Path.DirectorySeparatorChar & strPath2
            End If
        End If

        Return strPath
    End Function

    Public Function open_FileSystemObject(ByVal objOItem_FileSystemObject As clsOntologyItem) As clsOntologyItem
        Dim objOItem_Result As clsOntologyItem


        Select Case objOItem_FileSystemObject.GUID_Parent
            Case objLocalConfig.OItem_Type_Drive.GUID
                objOItem_FileSystemObject.Additional1 = get_Path_FileSystemObject(objOItem_FileSystemObject)
                objOItem_Result = objLocalConfig.Globals.LState_Success
                objShellWork.start_Process(objOItem_FileSystemObject.Additional1, Nothing, Nothing, False, False)
            Case objLocalConfig.OItem_type_Folder.GUID
                objOItem_FileSystemObject.Additional1 = get_Path_FileSystemObject(objOItem_FileSystemObject)
                objOItem_Result = objLocalConfig.Globals.LState_Success
                objShellWork.start_Process(objOItem_FileSystemObject.Additional1, Nothing, Nothing, False, False)
            Case objLocalConfig.OItem_Type_File.GUID
                objOItem_FileSystemObject.Additional1 = get_Path_FileSystemObject(objOItem_FileSystemObject)
                If boolBlob = True Then
                    objOItem_Result = open_BlobFile(objOItem_FileSystemObject)
                Else
                    objOItem_Result = objLocalConfig.Globals.LState_Success
                    objShellWork.start_Process(objOItem_FileSystemObject.Additional1, Nothing, Nothing, False, False)
                End If
            Case Else
                objOItem_Result = objLocalConfig.Globals.LState_Error
                objOItem_Result.Additional1 = "Beim Öffnen ist ein Fehler aufgetreten!"
        End Select

        Return objOItem_Result
    End Function

    Private Function open_BlobFile(ByVal objOItem_File As clsOntologyItem) As clsOntologyItem
        Dim objOItem_Result As clsOntologyItem
        Dim objDRC_FileCheckOut As DataRowCollection

        Dim strPath As String
        Dim strExtension As String
        Dim boolGoOn As Boolean

        objOItem_Result = objFrmBlobWatcher.IsFileCheckedout(objOItem_File)
        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Nothing.GUID Then

            objFrmBlobWatcher = New frmBlobWatcher(objLocalConfig.Globals)
            objOItem_Result = objFrmBlobWatcher.Initialize_BlobDirWatcher()

            If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then

                strExtension = ""
                If objOItem_File.Name.Contains(".") Then
                    strExtension = objOItem_File.Name.Substring(objOItem_File.Name.LastIndexOf("."))
                End If
                objOItem_File.Additional1 = merge_paths(objFrmBlobWatcher.PathBlobWatcher, objOItem_File.GUID.ToString & strExtension)
                objOItem_Result = objMediaStoreConnector.SaveManagedMediaToFile(objOItem_File, objOItem_File.Additional1)
                If objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
                    objOItem_Result.Additional1 = "Die Datei kann nicht geöffnet werden!"
                Else
                    objShellWork.start_Process(objOItem_File.Additional1, Nothing, Nothing, False, False)
                End If

            Else
                objOItem_Result = objLocalConfig.Globals.LState_Error
                objOItem_Result.Additional1 = "Die Datei kann nicht geöffnet werden, weil der Blob-Watcher nicht startet!"

            End If


        ElseIf objOItem_Result.GUID = objLocalConfig.Globals.LState_Relation.GUID Then

            objOItem_Result = objLocalConfig.Globals.LState_Error
            objOItem_Result.Additional1 = "Die Datei ist bereits am Server " & objDRC_FileCheckOut(0).Item("Name_Server") & " geöffnet!"
        End If
        Return objOItem_Result
    End Function


    Public Function IntegrateBlobs(strPath As String) As clsOntologyItem
        Dim objOItem_Result As clsOntologyItem = objLocalConfig.Globals.LState_Success.Clone()
        Dim searchFiles = New List(Of clsOntologyItem)
        Dim filesToIntegrate = New List(Of clsOntologyItem)

        For Each strFile As String In IO.Directory.GetFiles(strPath)
            Dim strFileName = IO.Path.GetFileName(strFile)
            If objLocalConfig.Globals.is_GUID(strFileName) Then
                If Not searchFiles.Any(Function(f) f.GUID = strFileName) Then
                    searchFiles.Add(New clsOntologyItem With {.GUID = strFileName,
                                                              .Additional1 = strFile})

                End If
            End If
        Next

        If searchFiles.Any() Then
            If searchFiles.Count < 500 Then
                objOItem_Result = objDBLevel_Blob.GetDataObjects(searchFiles)
            Else
                objOItem_Result = objDBLevel_Blob.GetDataObjects(New List(Of clsOntologyItem) From {New clsOntologyItem With {.GUID_Parent = objLocalConfig.OItem_Type_File.GUID}})
            End If

            If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
                Dim objOList_FilesFound = objDBLevel_Blob.Objects1.Select(Function(f) New clsOntologyItem With
                                                                                           {.GUID = f.GUID,
                                                                                            .Name = f.Name,
                                                                                            .GUID_Parent = f.GUID_Parent,
                                                                                            .Type = objLocalConfig.Globals.Type_Object,
                                                                                            .Mark = is_File_Blob(f.GUID)}).Where(Function(f) f.Mark = False).ToList()
                filesToIntegrate = (From objFileDB In objOList_FilesFound
                                    Join objFile In searchFiles On objFileDB.GUID Equals objFile.GUID
                                    Select New clsOntologyItem With {
                                        .GUID = objFileDB.GUID,
                                        .Name = objFileDB.Name,
                                        .GUID_Parent = objFileDB.GUID_Parent,
                                        .Type = objFileDB.Type,
                                        .Additional1 = objFile.Additional1}).ToList()

                For Each objFile In filesToIntegrate
                    objOItem_Result = objMediaStoreConnector.SaveFileToManagedMedia(objFile, objFile.Additional1)
                    If objOItem_Result.GUID = objLocalConfig.Globals.LState_Error.GUID Then
                        Exit For
                    End If
                Next

            End If
        Else
            objOItem_Result = objLocalConfig.Globals.LState_Nothing.Clone()
        End If

        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
            objOItem_Result.Count = searchFiles.Count - filesToIntegrate.Count
        End If

        Return objOItem_Result
    End Function

    Public Function InitializeZipFile(strPathZip As String) As clsOntologyItem
        Dim objOItem_Result = objLocalConfig.Globals.LState_Success.Clone()

        objZipFile = New Zip.ZipFile(strPathZip)

        Return objOItem_Result
    End Function

    Public Function FinalizeZipFile() As clsOntologyItem
        Dim objOItem_Result = objLocalConfig.Globals.LState_Success.Clone()

        Try
            objZipFile.Save()


        Catch ex As Exception
            objOItem_Result = objLocalConfig.Globals.LState_Error.Clone()
        End Try

        objZipFile = Nothing

        Return objOItem_Result
    End Function

    Public Function ExportFilesToZip(OList_Files As List(Of clsOntologyItem), strPathZip As String, boolGuidAsName As Boolean) As clsOntologyItem
        Dim objOItem_Result = objLocalConfig.Globals.LState_Success.Clone()
        Dim strFileName As String
        Dim strFilePathDst As String
        Dim strTempPath = Environment.ExpandEnvironmentVariables("%TEMP%")
        Dim boolSave As Boolean = False


        If objZipFile Is Nothing Then
            objOItem_Result = InitializeZipFile(strPathZip)
            boolSave = True
        End If

        If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
            For Each objOItem_File In OList_Files
                Dim strFilePath = objMediaStoreConnector.GetMediaBlobPath(objOItem_File)

                If Not strFilePath Is Nothing Then
                    If boolGuidAsName Then
                        strFileName = objOItem_File.GUID
                    Else
                        strFileName = objOItem_File.Name
                    End If


                    Try
                        Dim objZipEntry = objZipFile.AddFile(strFilePath)
                        objZipEntry.FileName = strFileName
                        'Using fsSource As IO.FileStream = New IO.FileStream(strFilePath, _
                        '    IO.FileMode.Open, IO.FileAccess.Read)
                        '    If objZipFile Is Nothing Then
                        '        objZipFile = New Zip.ZipFile(strPathZip)
                        '    Else
                        '        objZipFile = Zip.ZipFile.Read(strPathZip)
                        '    End If
                        '    objZipFile.AddEntry(strFileName, fsSource)

                        '    '' Read the source file into a byte array.
                        '    'Dim bytes() As Byte = New Byte((fsSource.Length) - 1) {}
                        '    'Dim numBytesToReadTotal As Integer = CType(fsSource.Length, Integer)
                        '    'Dim numBytesRead As Integer = 0
                        '    'Dim numBytesToRead As Integer = 30000
                        '    'Dim objZipEntry As Zip.ZipEntry = Nothing

                        '    'While (numBytesToReadTotal > 0)
                        '    '    ' Read may return anything from 0 to numBytesToRead.
                        '    '    Dim n As Integer = fsSource.Read(bytes, numBytesRead, _
                        '    '        numBytesToRead)
                        '    '    ' Break when the end of the file is reached.
                        '    '    If (n = 0) Then
                        '    '        Exit While
                        '    '    End If
                        '    '    numBytesRead = (numBytesRead + n)
                        '    '    numBytesToReadTotal = (numBytesToReadTotal - n)
                        '    '    If objZipEntry Is Nothing Then
                        '    '        objZipEntry = objZipFile.AddEntry(strFileName, bytes)
                        '    '    Else

                        '    '    End If


                        '    'End While



                        'End Using
                    Catch ioEx As IO.FileNotFoundException
                        objOItem_Result = objLocalConfig.Globals.LState_Error.Clone()
                        Exit For
                    End Try



                    'strFilePathDst = strTempPath & "\" & strFileName
                    'objOItem_Result = objBlobConnection.save_Blob_To_File(objOItem_File, strFilePathDst, True)
                    'If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then
                    '    InitializeZipExport(strPathZip)
                    '    objZipFile.AddFile(strFilePathDst)

                    'End If
                End If

                'IO.File.Delete(strFilePathDst)

            Next

            If objOItem_Result.GUID = objLocalConfig.Globals.LState_Success.GUID Then

                If boolSave Then
                    objZipFile.Save()
                End If


                If Not objZipFile Is Nothing Then
                    objOItem_Result.Count = OList_Files.Count - objZipFile.Count

                Else
                    objOItem_Result.Count = OList_Files.Count
                End If

                If boolSave Then
                    FinalizeZipFile()
                End If

            End If
        End If



        Return objOItem_Result
    End Function


    Public Sub New(ByVal Globals As Globals)
        objLocalConfig = LocalConfigManager.GetLocalConfig(DirectCast(Assembly.GetExecutingAssembly().GetCustomAttributes(True).FirstOrDefault(Function(objAttribute) TypeOf (objAttribute) Is GuidAttribute), GuidAttribute).Value)
        If objLocalConfig Is Nothing Then
            objLocalConfig = New clsLocalConfig(Globals)
            LocalConfigManager.AddLocalConfig(objLocalConfig)
        End If

        set_DBConnection()
    End Sub

    Public Sub New(ByVal LocalConfig As clsLocalConfig)
        objLocalConfig = LocalConfig

        set_DBConnection()
        initialize()
    End Sub

    Private Sub initialize()
        objDataWork = New clsDataWork(objLocalConfig)
    End Sub

    Private Sub set_DBConnection()
        objShellWork = New clsShellWork()
        objTransaction = New clsTransaction(objLocalConfig.Globals)
        objRelationConfig = New clsRelationConfig(objLocalConfig.Globals)

        objDBLevel_FSO = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_Folder = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_Server = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_Drive = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_Blob = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_Share = New OntologyModDBConnector(objLocalConfig.Globals)
        objDBLevel_CreationDate = New OntologyModDBConnector(objLocalConfig.Globals)

        objMediaStoreConnector = New MediaStoreConnector(objLocalConfig.Globals)
        objFileWorkManager = New FileWorkManager(objLocalConfig.Globals)
    End Sub
End Class
