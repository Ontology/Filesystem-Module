﻿Imports System.Reflection
Imports Ontology_Module
Imports OntologyAppDBConnector
Imports OntoMsg_Module
Imports System.Runtime.InteropServices

Public Class frmFileBlobSync

    Private objUserControl_FileBlobSync As UserControl_FileBlobSync

    Private objLocalConfig As clsLocalConfig

    Private Sub ToolStripButton_Close_Click(sender As Object, e As EventArgs) Handles ToolStripButton_Close.Click
        Me.Close()
    End Sub

    Public Sub New(Globals As Globals)

        ' Dieser Aufruf ist für den Designer erforderlich.
        InitializeComponent()

        ' Fügen Sie Initialisierungen nach dem InitializeComponent()-Aufruf hinzu.
        objLocalConfig = LocalConfigManager.GetLocalConfig(DirectCast(Assembly.GetExecutingAssembly().GetCustomAttributes(True).FirstOrDefault(Function(objAttribute) TypeOf (objAttribute) Is GuidAttribute), GuidAttribute).Value)
        If objLocalConfig Is Nothing Then
            objLocalConfig = New clsLocalConfig(Globals)
            LocalConfigManager.AddLocalConfig(objLocalConfig)
        End If

        Initialize()
    End Sub

    Public Sub New(LocalConfig As clsLocalConfig)

        ' Dieser Aufruf ist für den Designer erforderlich.
        InitializeComponent()

        ' Fügen Sie Initialisierungen nach dem InitializeComponent()-Aufruf hinzu.
        objLocalConfig = LocalConfig

        Initialize()
    End Sub

    Private Sub Initialize()
        objUserControl_FileBlobSync = New UserControl_FileBlobSync(objLocalConfig)
        objUserControl_FileBlobSync.Dock = DockStyle.Fill
        ToolStripContainer1.ContentPanel.Controls.Add(objUserControl_FileBlobSync)
    End Sub
End Class